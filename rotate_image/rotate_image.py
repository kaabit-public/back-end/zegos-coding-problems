"""
ASKED BY AMAZON, MICROSOFT AND GOOGLE
Note: Try to solve this task in-place (with O(1) additional memory), since this is what you'll be asked to do during an interview.

You are given an n x n 2D matrix that represents an image. Rotate the image by 90 degrees (clockwise).

Example

For

a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

the output should be

rotateImage(a) =
    [[7, 4, 1],
     [8, 5, 2],
     [9, 6, 3]]

Input/Output

    [execution time limit] 4 seconds (py3)

    [input] array.array.integer a

    Guaranteed constraints:
    1 ≤ a.length ≤ 100,
    a[i].length = a.length,
    1 ≤ a[i][j] ≤ 104.

    [output] array.array.intege
"""

def rotateImage(a):
    matrix_len = len(a)
    new_matrix = []

    for i in range(matrix_len):
        dummy_data = ' ' * (matrix_len - 1)
        new_matrix.append(dummy_data.split(' '))

    count = matrix_len - 1
    aux_count = 0
    while count >= 0:
        for i in range(matrix_len):
            new_matrix[i][count] = a[aux_count][i]

        aux_count += 1
        count -= 1
    
    return new_matrix

# if __name__ == '__main__':
def main(matrix):
    # matrix = input('Ingresa la matriz a rotar: ')

    return rotateImage(matrix)
