# --- Directions
# Write a function that accepts a positive number N.
# The function should console log a step shape
# with N levels using the # character.  Make sure the
# step has spaces on the right hand side!
# --- Examples
#   steps(2)
#       '# '
#       '##'
#   steps(3)
#       '#  '
#       '## '
#       '###'
#   steps(4)
#       '#   '
#       '##  '
#       '### '
#       '####'

def steps(n):
    pattern = []
    for i in range(n):
        result = ''
        result += '*' * (i + 1)
        result += ' ' * (n - (i + 1))
        pattern.append(result)

    return pattern 

# if __name__ == '__main__':
def main(n):
    # n = int(input('Ingrese un numero: '))

    return steps(n)