def make_perfect_string(s, position=0, count=0):
    if position == len(s):
        return count

    if position < len(s) - 1:
        if s[position] == s[position + 1]:
            first_part = s[:position]
            second_part = s[position+1:]
            s = first_part + second_part
            return make_perfect_string(s, position, count + 1)

    return make_perfect_string(s, position + 1, count)

def alternatingCharacters(s):
    # RECURSIVE
    return make_perfect_string(s)

    # NON RECURSIVE
    # count = 0
    # for i in range(1, len(s)):
    #     if s[i] == s[i - 1]:
    #         count += 1

    # return count


# if __name__ == '__main__':
def main(strings):
    # q = int(input())
    result = []
    for string in strings:
        # s = input()

        result.append(alternatingCharacters(string))

    return result
