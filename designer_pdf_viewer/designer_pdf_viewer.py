def designerPdfViewer(h, word):
    ABC = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0, 'I': 0, 'J': 0, 'K': 0, 'L': 0, 'M': 0, 'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0, 'S': 0, 'T': 0, 'U': 0, 'V': 0, 'W': 0, 'X': 0, 'Y': 0, 'Z': 0}
    count = 0

    for key in ABC.keys():
        ABC[key] = h[count]
        count += 1

    characters_weight = [ABC[character.upper()] for character in word]

    return (len(word) * 1) * max(characters_weight)

# if __name__ == '__main__':
def main(weigths, word):
    # h = list(map(int, input().rstrip().split()))

    # word = input()

    return designerPdfViewer(weigths, word)
