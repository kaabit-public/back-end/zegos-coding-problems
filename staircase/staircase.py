def staircase_right_aligned(n):
    stair = []
    for i in range(n):
        stair.append(' '*(n - i - 1) + '#'*(i + 1))

    return stair

# if __name__ == '__main__':
def main(n):
    # n = int(input())

    return staircase_right_aligned(n)
