def plusMinus(arr):
    positives = [number for number in arr if number > 0]
    negatives = [number for number in arr if number < 0]
    zeros  = [number for number in arr if number == 0]

    positive_proportion = "{0:.6f}".format(len(positives)/len(arr))
    negative_proportion = "{0:.6f}".format(len(negatives)/len(arr))
    zero_proportion = "{0:.6f}".format(len(zeros)/len(arr))

    return [
        positive_proportion,
        negative_proportion,
        zero_proportion
    ]

# if __name__ == '__main__':
def main(arr):
    # n = int(input())

    # arr = list(map(int, input().rstrip().split()))

    return plusMinus(arr)