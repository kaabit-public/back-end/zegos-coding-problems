def bubble_sort(candles):
    if len(candles) == 1:
        return candles

    for i in range(len(candles)):
        for j in range(i + 1, len(candles)):
            if candles[i] < candles[j]:
                aux = candles[i]
                candles[i] = candles[j]
                candles[j] = aux

    return candles

def birthdayCakeCandles(candles):
    """ The normal but not the most efficient way """
    # candles_set = list(set(candles))

    # sorted_candles = bubble_sort(candles_set)

    # max_candle_height = sorted_candles[0]

    # max_occurrences = [candle for candle in candles if candle == max_candle_height]

    # return len(max_occurrences)
    
    """ The most efficient way """
    max_candle_height = max(candles)

    max_occurrences = [candle for candle in candles if candle == max_candle_height]

    return len(max_occurrences)


# if __name__ == '__main__':
def main(candles):
    # candles_count = int(input().strip())

    # candles = list(map(int, input().rstrip().split()))

    result = birthdayCakeCandles(candles)

    return result
