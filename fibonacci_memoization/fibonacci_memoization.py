def memoize(func):
    cache = {}

    def wrapper(*args):
        if args[0] in list(cache.keys()):
            return cache[args[0]];

        result = func(args[0]);
        cache[args[0]] = result

        return result

    return wrapper

def fibonacci(n):
    if n == 0:
        return 0

    if n == 1:
        return 1

    return fibonacci(n - 1) + fibonacci(n - 2)

fibonacci = memoize(fibonacci)

def main(n):
    global fibonacci
    return fibonacci(n)
