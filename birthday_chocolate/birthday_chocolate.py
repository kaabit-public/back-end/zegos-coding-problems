def birthday(s, d, m):
    count = 0
    for i in range(len(s)):
        if len(s) < m:
            break

        if sum(s[i:i + m]) == d:
            count += 1

    return count

# if __name__ == '__main__':
def main(s_input, dm_input):
    # n = int(input().strip())
    s = list(map(int, s_input.split()))

    dm = dm_input.split()
    d = int(dm[0])
    m = int(dm[1])

    return birthday(s, d, m)