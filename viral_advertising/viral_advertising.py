import math

def viralAdvertising(n):
    recipients = 5
    results = []

    for i in range(1, n + 1):
        people_liked_advertisement = math.floor(recipients / 2)
        total_shares = people_liked_advertisement * 3
        results.append(people_liked_advertisement)
        recipients = total_shares

    return sum(results)

# if __name__ == '__main__':
def main(n):
    # n = int(input())

    return viralAdvertising(n)
