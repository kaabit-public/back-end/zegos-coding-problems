#  Python utilities
import unittest

# Coding problem
from encryption.encryption import main


class EncryptionTests(unittest.TestCase):

    def test_case_one(self):
        string = 'if man was meant to stay on the ground god would have given us roots'
        self.assertEqual(main(string), 'imtgdvsg fearweri mayoogov anouuioe ntnnlvtn wttddes  aohghnu sseoaus ')

    def test_case_two(self):
        string = 'chillout'
        self.assertEqual(main(string), 'clu hlt io ')

    def test_case_three(self):
        string = 'haveaniceday'
        self.assertEqual(main(string), 'hae and via ecy ')


if __name__ == '__main__':
    unittest.main()