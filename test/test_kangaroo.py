#  Python utilities
import unittest

# Coding problem
from kangaroo.kangaroo import main


class KangarooTests(unittest.TestCase):

    def test_case_one(self):
        info = '4523 8092 9419 8076'
        self.assertEqual(main(info), 'YES')

    def test_case_two(self):
        info = '0 2 5 3'
        self.assertEqual(main(info), 'NO')

    def test_case_three(self):
        info = '45 7 56 2'
        self.assertEqual(main(info), 'NO')

    def test_case_four(self):
        info = '43 2 70 2'
        self.assertEqual(main(info), 'NO')


if __name__ == '__main__':
    unittest.main()