#  Python utilities
import unittest

# Coding problem
from valid_parentheses.valid_parentheses import valid_parentheses


class PersistenBuggerTests(unittest.TestCase):

    def test_case_one(self):
        self.assertEqual(valid_parentheses(""), True)

    def test_case_two(self):
        self.assertEqual(valid_parentheses("(())((()())())"), False)

    def test_case_three(self):
        self.assertEqual(valid_parentheses("h(l(n)dq)(j(pt))(czhvp)lkttrs"), True)

    def test_case_fout(self):
        self.assertEqual(valid_parentheses("mbd()lcy)q(gnt(jxz(wu(u)d))hwfvijbh"), False)


if __name__ == '__main__':
    unittest.main()