#  Python utilities
import unittest

# Coding problem
from caesar_cipher.caesar_cipher import main


class CaesarCipherTests(unittest.TestCase):

    def test_case_one(self):
        string = 'middle-Outz'
        k = 2
        self.assertEqual(main(string, k), 'okffng-Qwvb')

    def test_case_two(self):
        string = 'Always-Look-on-the-Bright-Side-of-Life'
        k = 5
        self.assertEqual(main(string, k), 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj')

    def test_case_three(self):
        string = '159357lcfd'
        k = 98
        self.assertEqual(main(string, k), '159357fwzx')


if __name__ == '__main__':
    unittest.main()