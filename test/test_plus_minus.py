#  Python utilities
import unittest

# Coding problem
from plus_minus.plus_minus import main


class PlusMinusTests(unittest.TestCase):

    def test_case_one(self):
        array = [1, 2, 3, -1, -2, -3, 0, 0]
        self.assertEqual(main(array), ['0.375000', '0.375000', '0.250000'])

    def test_case_two(self):
        array = [-4, 3, -9, 0, 4, 1]
        self.assertEqual(main(array), ['0.500000', '0.333333', '0.166667'])

    def test_case_three(self):
        array = [-100, 100, 0, 0, 0, -100, 100, 0, -100, 100, 100, 0, 0, 0, 0, -100, -100, -100, 0, -100, 0, 100, 100, -100, -100, 100, 100, 100, 100, -100, -100, -100, -100, 100, 0, 0, 100, 0, 0, -100, -100, -100, -100, -100, -100, 100, 100, 0, 100, 100, -100, -100, -100, 0, 100, -100, 0, 100, 100, -100, 100, -100, 0, -100, -100, 100, 0, 0, -100, 0, -100, -100, 100, -100, 100, 0, 100, -100, -100, -100, 100, 100, 100, 100, 0, -100, 0, 100, 100, 100, 0, -100, -100, 0, 0, 100, 0, -100, 100, 100]
        self.assertEqual(main(array), ['0.340000', '0.380000', '0.280000'])


if __name__ == '__main__':
    unittest.main()