#  Python utilities
import unittest

# Coding problem
from viral_advertising.viral_advertising import main


class ViralAdvertisingTests(unittest.TestCase):

    def test_case_one(self):
        n = 3
        result = 9
        self.assertEqual(main(n), result)

    def test_case_two(self):
        n = 4
        result = 15
        self.assertEqual(main(n), result)

    def test_case_three(self):
        n = 34
        result = 3149621
        self.assertEqual(main(n), result)


if __name__ == '__main__':
    unittest.main()
