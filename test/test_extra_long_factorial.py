#  Python utilities
import unittest

# Coding problem
from extra_long_factorial.extra_long_factorial import main


class ExtraLongFactorialTests(unittest.TestCase):

    def test_case_one(self):
        n = 25
        self.assertEqual(main(n), 15511210043330985984000000)

    def test_case_two(self):
        n = 30
        self.assertEqual(main(n), 265252859812191058636308480000000)


if __name__ == '__main__':
    unittest.main()