#  Python utilities
import unittest

# Coding problem
from compare_triplets.compare_triplets import main


class CompareTripletsTests(unittest.TestCase):

    def test_case_one(self):
        alice = [17, 28, 30]
        bob = [99, 16, 8]
        self.assertEqual(main(alice, bob), [2, 1])

    def test_case_two(self):
        alice = [5, 6, 7]
        bob = [3, 6, 10]
        self.assertEqual(main(alice, bob), [1, 1])

    def test_case_three(self):
        alice = [17, 28, 30]
        bob = [99, 16, 8]
        self.assertEqual(main(alice, bob), [2, 1])

if __name__ == '__main__':
    unittest.main()