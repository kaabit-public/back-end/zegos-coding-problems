#  Python utilities
import unittest

# Coding problem
from find_parity_outlier.find_the_parity_outlier import find_outlier


class FindTheParityOutlierTests(unittest.TestCase):

    def test_case_one(self):
        self.assertEqual(find_outlier([2, 4, 6, 8, 10, 3]), 3)

    def test_case_two(self):
        self.assertEqual(find_outlier([2, 4, 0, 100, 4, 11, 2602, 36]), 11)

    def test_case_three(self):
        self.assertEqual(find_outlier([160, 3, 1719, 19, 11, 13, -21]), 160)


if __name__ == '__main__':
    unittest.main()