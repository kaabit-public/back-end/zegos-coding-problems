#  Python utilities
import unittest

# Coding problem
from beautiful_days_at_the_movies.beautiful_days_at_the_movies import main


class BeautifulDaysTests(unittest.TestCase):

    def test_case_one(self):
        i = 20
        j = 23
        k = 6
        self.assertEqual(main(i, j, k), 2)

    def test_case_two(self):
        i = 13
        j = 45
        k = 3
        self.assertEqual(main(i, j, k), 33)


if __name__ == '__main__':
    unittest.main()
