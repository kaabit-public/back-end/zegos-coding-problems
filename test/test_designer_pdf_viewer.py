#  Python utilities
import unittest

# Coding problem
from designer_pdf_viewer.designer_pdf_viewer import main


class DesignerPDFViewerTests(unittest.TestCase):

    def test_case_one(self):
        weigths = [1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
        word = 'abc'
        self.assertEqual(main(weigths, word), 9)

    def test_case_two(self):
        weigths = [1,3,1,3,1,4,1,3,2,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,7]
        word = 'zaba'
        self.assertEqual(main(weigths, word), 28)


if __name__ == '__main__':
    unittest.main()