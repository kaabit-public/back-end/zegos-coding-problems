#  Python utilities
import unittest

# Coding problem
from utopian_tree.utopian_tree import main


class UtopianTreeTests(unittest.TestCase):

    def test_case_one(self):
        periods = [0, 1, 2, 3, 4, 5]
        self.assertEqual(main(periods), [1, 2, 3, 6, 7, 14])

    def test_case_two(self):
        periods = [0, 1, 4]
        self.assertEqual(main(periods), [1, 2, 7])


if __name__ == '__main__':
    unittest.main()