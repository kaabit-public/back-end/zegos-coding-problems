#  Python utilities
import unittest

# Coding problem
from steps.steps import main


class StepsTests(unittest.TestCase):

    def test_case_one(self):
        n = 5
        result = [
            '*    ',
            '**   ',
            '***  ',
            '**** ',
            '*****'
        ]
        self.assertEqual(main(n), result)

    def test_case_two(self):
        n = 10
        result = [
            '*         ',
            '**        ',
            '***       ',
            '****      ',
            '*****     ',
            '******    ',
            '*******   ',
            '********  ',
            '********* ',
            '**********'
        ]
        self.assertEqual(main(n), result)

    def test_case_three(self):
        n = 15
        result = [
            '*              ',
            '**             ',
            '***            ',
            '****           ',
            '*****          ',
            '******         ',
            '*******        ',
            '********       ',
            '*********      ',
            '**********     ',
            '***********    ',
            '************   ',
            '*************  ',
            '************** ',
            '***************'
        ]
        self.assertEqual(main(n), result)


if __name__ == '__main__':
    unittest.main()