#  Python utilities
import unittest

# Coding problem
from spiral_matrix.spiral_matrix import main


class SpiralMatrixTests(unittest.TestCase):

    def test_case_one(self):
        n = 3
        result = [
            [1, 2, 3],
            [8, 9, 4],
            [7, 6, 5]
        ]
        self.assertEqual(main(n), result)

    def test_case_two(self):
        n = 4
        result = [
            [1,  2,  3, 4],
            [12, 13, 14, 5],
            [11, 16, 15, 6],
            [10,  9,  8, 7]
        ]
        self.assertEqual(main(n), result)

    def test_case_three(self):
        n = 5
        result = [
            [ 1,  2,  3,  4, 5],
            [16, 17, 18, 19, 6],
            [15, 24, 25, 20, 7],
            [14, 23, 22, 21, 8],
            [13, 12, 11, 10, 9]
        ]
        self.assertEqual(main(n), result)


if __name__ == '__main__':
    unittest.main()