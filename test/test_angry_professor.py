#  Python utilities
import unittest

# Coding problem
from angry_professor.angry_professor import main


class AngryProffesorTests(unittest.TestCase):

    def test_case_one(self):
        k = 3
        a = [-1, -3, 4, 2]
        self.assertEqual(main(k, a), 'YES')

    def test_case_two(self):
        k = 2
        a =[0, -1, 2, 1]
        self.assertEqual(main(k, a), 'NO')


if __name__ == '__main__':
    unittest.main()