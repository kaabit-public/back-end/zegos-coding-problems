#  Python utilities
import unittest

# Coding problem
from breaking_records.breaking_records import main


class BirthdayChocolateTests(unittest.TestCase):

    def test_case_one(self):
        scores = '10 5 20 20 4 5 2 25 1'
        self.assertEqual(main(scores), [2, 4])

    def test_case_two(self):
        scores = '3 4 21 36 10 28 35 5 24 42'
        self.assertEqual(main(scores), [4, 0])

    def test_case_three(self):
        scores = '17 45 41 60 17 41 76 43 51 40 89 92 34 6 64 7 37 81 32 50'
        self.assertEqual(main(scores), [5, 1])

if __name__ == '__main__':
    unittest.main()