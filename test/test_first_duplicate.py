#  Python utilities
import unittest

# Coding problem
from first_duplicate.first_duplicate import main


class FirstDuplicateTests(unittest.TestCase):

    def test_case_one(self):
        result = 6
        self.assertEqual(main([8, 4, 6, 2, 6, 4, 7, 9, 5, 8]), result)

    def test_case_two(self):
        result = 3
        self.assertEqual(main([2, 1, 3, 5, 3, 2]), result)

    def test_case_three(self):
        result = 1
        self.assertEqual(main([1, 1, 2, 2, 1]), result)


if __name__ == '__main__':
    unittest.main()
