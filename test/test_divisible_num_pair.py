#  Python utilities
import unittest

# Coding problem
from divisible_num_paiir.divisible_num_pair import main


class DivisibleNumPairTests(unittest.TestCase):

    def test_case_one(self):
        array = [1, 3, 2, 6, 1, 2]
        nk = '6 3'
        self.assertEqual(main(nk, array), 5)

if __name__ == '__main__':
    unittest.main()