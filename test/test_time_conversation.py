#  Python utilities
import unittest

# Coding problem
from time_conversion.time_conversion import main


class TimeConversionTests(unittest.TestCase):

    def test_case_one(self):
        string = "07:05:45PM"
        self.assertEqual(main(string), "19:05:45")

    def test_case_two(self):
        string = "12:45:54PM"
        self.assertEqual(main(string), "12:45:54")

    def test_case_three(self):
        string = "11:59:59PM"
        self.assertEqual(main(string), "23:59:59")

if __name__ == '__main__':
    unittest.main()