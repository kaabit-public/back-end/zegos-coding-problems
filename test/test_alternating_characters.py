#  Python utilities
import unittest

# Coding problem
from alternating_characters.alternating_characters import main


class AlternatingCharactersTests(unittest.TestCase):

    def test_case_one(self):
        strings = [
            'AAAA',
            'BBBBB',
            'ABABABAB',
            'BABABA',
            'AAABBB'
        ]
        result = [
            3,
            4,
            0,
            0,
            4
        ]
        self.assertEqual(main(strings), result)

    def test_case_two(self):
        strings = [
            'AAABBBAABB',
            'AABBAABB',
            'ABABABAA'
        ]
        result = [
            6,
            4,
            1
        ]
        self.assertEqual(main(strings), result)

    def test_case_three(self):
        strings = [
            'ABBABBAA'
        ]
        result = [
            3
        ]
        self.assertEqual(main(strings), result)


if __name__ == '__main__':
    unittest.main()
