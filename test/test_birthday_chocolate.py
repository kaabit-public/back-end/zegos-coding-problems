#  Python utilities
import unittest

# Coding problem
from birthday_chocolate.birthday_chocolate import main


class BirthdayChocolateTests(unittest.TestCase):

    def test_case_one(self):
        s = '1 2 1 3 2'
        dm = '3 2'
        self.assertEqual(main(s, dm), 2)

    def test_case_two(self):
        s = '1 1 1 1 1 1'
        dm = '3 2'
        self.assertEqual(main(s, dm), 0)

    def test_case_three(self):
        s = '4'
        dm = '4 1'
        self.assertEqual(main(s, dm), 1)

if __name__ == '__main__':
    unittest.main()