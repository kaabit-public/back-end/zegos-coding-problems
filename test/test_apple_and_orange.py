#  Python utilities
import unittest

# Coding problem
from apple_and_orange.apple_and_orange import main


class AppleAndOrangeTests(unittest.TestCase):

    def test_case_one(self):
        st ='7 11'
        ab = '5 15'
        mn = '3 2'
        apples = '-2 2 1'
        oranges = '5 -6'
        self.assertEqual(main(st, ab, mn, apples, oranges), [1, 1])


if __name__ == '__main__':
    unittest.main()