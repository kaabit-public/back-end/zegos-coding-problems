#  Python utilities
import unittest

# Coding problem
from first_not_reapiting_character.first_not_reapiting_character import main


class FirstNotReapitingCharacterTests(unittest.TestCase):

    def test_case_one(self):
        result = '_'
        self.assertEqual(main('abacabaabacaba'), result)

    def test_case_two(self):
        result = 'g'
        self.assertEqual(main('ngrhhqbhnsipkcoqjyviikvxbxyphsnjpdxkhtadltsuxbfbrkof'), result)

    def test_case_three(self):
        result = 'd'
        self.assertEqual(main('xdnxxlvupzuwgigeqjggosgljuhliybkjpibyatofcjbfxwtalc'), result)


if __name__ == '__main__':
    unittest.main()
