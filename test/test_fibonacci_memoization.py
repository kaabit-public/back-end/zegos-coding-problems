#  Python utilities
import unittest

# Coding problem
from fibonacci_memoization.fibonacci_memoization import main


class FibonacciMemoizationTests(unittest.TestCase):

    def test_case_one(self):
        result = 12586269025
        self.assertEqual(main(50), result)

    def test_case_two(self):
        result = 55
        self.assertEqual(main(10), result)

    def test_case_three(self):
        result = 354224848179261915075
        self.assertEqual(main(100), result)


if __name__ == '__main__':
    unittest.main()
