#  Python utilities
import unittest

# Coding problem
from persistent_bugger.persisten_bugger import persistence


class PersistenBuggerTests(unittest.TestCase):

    def test_case_one(self):
        self.assertEqual(persistence(39), 3)

    def test_case_two(self):
        self.assertEqual(persistence(4), 0)

    def test_case_three(self):
        self.assertEqual(persistence(25), 2)

    def test_case_fout(self):
        self.assertEqual(persistence(999), 4)


if __name__ == '__main__':
    unittest.main()