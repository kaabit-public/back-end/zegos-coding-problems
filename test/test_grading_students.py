#  Python utilities
import unittest

# Coding problem
from grading_students.grading_students import main


class GradingStudentsTests(unittest.TestCase):

    def test_case_one(self):
        grades = [73,67,38,33]
        self.assertEqual(main(grades), [75,67,40,33])

    def test_case_two(self):
        grades = [36, 6, 98, 25, 97, 24, 25, 70, 50, 71, 30, 14, 28, 100, 3, 26, 61, 98, 50, 41, 5, 3, 28, 34, 0]
        self.assertEqual(main(grades), [36, 6, 100, 25, 97, 24, 25, 70, 50, 71, 30, 14, 28, 100, 3, 26, 61, 100, 50, 41, 5, 3, 28, 34, 0])

    def test_case_three(self):
        grades = [59, 36, 97, 28, 61, 54, 27, 14, 29, 81, 16, 7, 1, 99, 42, 77, 39, 20, 29, 0, 1, 82, 20, 71, 71, 73, 79, 77, 61, 7, 93, 36, 65, 11, 92, 87, 85, 62, 45, 33, 9, 6, 37, 31, 67, 32, 67, 73, 59, 95]
        self.assertEqual(main(grades), [60, 36, 97, 28, 61, 55, 27, 14, 29, 81, 16, 7, 1, 100, 42, 77, 40, 20, 29, 0, 1, 82, 20, 71, 71, 75, 80, 77, 61, 7, 95, 36, 65, 11, 92, 87, 85, 62, 45, 33, 9, 6, 37, 31, 67, 32, 67, 75, 60, 95])


if __name__ == '__main__':
    unittest.main()