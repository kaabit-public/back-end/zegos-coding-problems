#  Python utilities
import unittest

# Coding problem
from count_duplicates.count_duplicates import duplicate_count


class CountDuplicatesTests(unittest.TestCase):

    def test_case_one(self):
        self.assertEqual(duplicate_count("abcde"), 0)

    def test_case_two(self):
        self.assertEqual(duplicate_count("abcdea"), 1)

    def test_case_three(self):
        self.assertEqual(duplicate_count("indivisibility"), 1)


if __name__ == '__main__':
    unittest.main()