#  Python utilities
import unittest

# Coding problem
from cut_the_sticks.cut_the_sticks import main


class CutTheSticksTests(unittest.TestCase):

    def test_case_one(self):
        array = [5, 4, 4, 2, 2, 8]
        self.assertEqual(main(array), [6, 4, 2, 1])

    def test_case_two(self):
        array = [1, 2, 3, 4, 3, 3, 2, 1]
        self.assertEqual(main(array), [8, 6, 4, 1])


if __name__ == '__main__':
    unittest.main()