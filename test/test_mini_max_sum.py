#  Python utilities
import unittest

# Coding problem
from mini_max_sum.mini_max_sum import main


class MiniMaxSumTests(unittest.TestCase):

    def test_case_one(self):
        array = [7, 69, 2, 221, 8974]
        self.assertEqual(main(array), [299, 9271])

    def test_case_two(self):
        array = [793810624, 895642170, 685903712, 623789054, 468592370]
        self.assertEqual(main(array), [2572095760, 2999145560])

    def test_case_three(self):
        array = [942381765, 627450398, 954173620, 583762094, 236817490]
        self.assertEqual(main(array), [2390411747, 3107767877])


if __name__ == '__main__':
    unittest.main()