#  Python utilities
import unittest

# Coding problem
from repeated_string.repeated_string import main


class RepeatedStringTests(unittest.TestCase):

    def test_case_one(self):
        string = 'aba'
        length = 10
        self.assertEqual(main(string, length), 7)

    def test_case_two(self):
        string = 'a'
        length = 1000000000000
        self.assertEqual(main(string, length), 1000000000000)

    def test_case_three(self):
        string = 'epsxyyflvrrrxzvnoenvpegvuonodjoxfwdmcvwctmekpsnamchznsoxaklzjgrqruyzavshfbmuhdwwmpbkwcuomqhiyvuztwvq'
        length = 549382313570
        self.assertEqual(main(string, length), 16481469408)


if __name__ == '__main__':
    unittest.main()
