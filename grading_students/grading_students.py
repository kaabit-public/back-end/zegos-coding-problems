def gradingStudents(grades):
    new_grades = []
    for grade in grades:
        if grade < 38 or grade == 100:
            new_grades.append(grade)
            continue

        num_list = [int(n) for n in str(grade)]
        if num_list[1] > 5:
            next_multiple_of_five = int(f'{num_list[0] + 1}0')
        else:
            next_multiple_of_five = int(f'{num_list[0]}5')

        if next_multiple_of_five - grade < 3:
            new_grades.append(next_multiple_of_five)
        else:
            new_grades.append(grade)

    return new_grades

# if __name__ == '__main__':
def main(grades):
    # grades_count = int(input().strip())

    # grades = []

    # for _ in range(grades_count):
    #     grades_item = int(input().strip())
    #     grades.append(grades_item)

    return gradingStudents(grades)