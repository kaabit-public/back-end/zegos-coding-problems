"""

    Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence, 
    which is the number of times you must multiply the digits in num until you reach a single digit.

"""

def persistence(n):
    numbers = list(map(int,' '.join(str(n)).split()))
    result = 1
    counter = 0

    if len(numbers) == 1:
        return 0

    while True:
        for number in numbers:
            result *= number
        counter += 1
        numbers = list(map(int, ' '.join(str(result)).split()))
        result = 1

        if len(numbers) == 1:
            return counter
            break