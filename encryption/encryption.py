import math

def string_into_matrix(rows, columns, string):
    matrix = []
    matrix_one = []
    count = 0
    for i in range(rows):
        for j in range(columns):
            if count < len(string):
                matrix_one.append(string[count])
                count += 1
        matrix.append(matrix_one)
        matrix_one = []
    return matrix

def fill_left(string, matrix):
    s = ''
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            s += matrix[i][j]

    if s == string:
        return matrix
    else:
        matrix.append([character for character in string[len(s):]])
        return matrix

def return_encoded_message(rows, columns, matrix):
    string = ''
    count = 0
    while count < columns:
        for i in range(len(matrix)):
            if len(matrix[i]) < columns:
                try:
                    if matrix[i][count] != '':
                        string += matrix[i][count]
                except:
                    pass
            else:
                string += matrix[i][count]
        string += ' '
        count += 1

    return string

def encryption(s):
    string_without_spaces = ''.join([character for character in s if character != ' '])

    rows = math.floor(math.sqrt(len(string_without_spaces)))
    columns = math.ceil(math.sqrt(len(string_without_spaces)))

    matrix_of_strings = string_into_matrix(rows, columns, string_without_spaces)
    matrix_of_strings = fill_left(s, matrix_of_strings)
    encoded_message = return_encoded_message(rows, columns, matrix_of_strings)

    return encoded_message

# if __name__ == '__main__':
def main(s):
    # s = input()

    result = encryption(s)

    return result
