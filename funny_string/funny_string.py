def funnyString(s):
    string = s
    reverse = s[::-1]

    string_ascii = [ord(char) for char in string]
    string_reversed_ascii = [ord(char) for char in reverse]

    string_ascii_difference = []
    for i in range(len(string_ascii) - 1):
        string_ascii_difference.append(abs(string_ascii[i] - string_ascii[i + 1]))

    for i in range(len(string_ascii) - 1):
        if abs(string_reversed_ascii[i] - string_reversed_ascii[i + 1]) != string_ascii_difference[i]:
            return 'Not Funny'

    return 'Funny'

# if __name__ == '__main__':
def main(strings):
    # q = int(input())
    result = []
    for string in strings:
        # s = input()
        result.append(funnyString(string))

    return result