def get_rankings(scores):
    ranking = [] # List of ranking
    count = 0 # Variable used to check if i have to skip positions
    position = 1 # Variable used to store the psotion of the score
    for i in range(len(scores)):
        if i != count:
        # Condition to check if i have to skip the actual i position by comparing i with count
            continue

        similar_scores = [score for score in scores if scores[i] == score]
        for similar_score in similar_scores:
            ranking.append(position)
        position += 1
        count += len(similar_scores)

    return ranking

def binary_search(array, value):
    low = 0
    high = len(array) - 1

    while (low < high):
        middle = low + (high - low) // 2
        if array[middle] == value:
            return middle
        elif array[middle] < value and value < array[middle - 1]:
            return middle
        elif array[middle] > value and value >= array[middle + 1]:
            return middle + 1
        elif array[middle] < value:
            high = middle - 1
        elif array[middle] > value:
            low = middle + 1

    return -1

"""
def get_alice_ranking(scores, alice_score):
    # This function is ok but with a large input (i.e. 10000) is not so efficient.
    # For this reason I´m using binary search and due to the array is already sorted.
    position = 0
    for i in range(len(scores) - 1, -1, -1):
        if alice_score <= scores[i]:
            position = i
            break
        else:
            position = -1

    return position
"""

def climbingLeaderboard(scores, alice):
    ranking = get_rankings(scores)
    alices_ranking = []
    for alice_score in alice:
        # ranking_position = get_alice_ranking(scores, alice_score)
        ranking_position = binary_search(scores, alice_score)
        if ranking_position == -1:
            if alice_score < scores[0]:
                # alices_ranking.append(str(ranking[-1] + 1))
                alices_ranking.append(int(ranking[-1] + 1))
            else:
                alices_ranking.append(int(1))
                # alices_ranking.append(str(1))
            # alices_ranking.append(str(1))
        else:
            aux = ranking[ranking_position] if alice_score >= scores[ranking_position] else ranking[ranking_position] + 1
            alices_ranking.append(int(aux))
            # alices_ranking.append(str(aux))

    return alices_ranking

def main(leaderboard_scores, alices_scores):
    # number_of_leaderboard_scores = int(input())
    # leaderboard_scores = list(map(int, input().rstrip().split()))
    # number_of_games_alice_plays = int(input())
    # alices_scores = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(leaderboard_scores, alices_scores)
    # print(result)
    # return '\n'.join(map(str, result))
    return result
