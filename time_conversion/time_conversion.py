HOUR_CHANGE = {
    '01': '13',
    '02': '14',
    '03': '15',
    '04': '16',
    '05': '17',
    '06': '18',
    '07': '19',
    '08': '20',
    '09': '21',
    '10': '22',
    '11': '23',
    '12': '12'
}

def timeConversion(s):
    seconds = s.split(":")[-1][:-2]

    if s.endswith('AM'):
        if s.split(':')[0] == '12':
            return f'00:{s.split(":")[1]}:{seconds}'

        return f'{":".join(s.split(":")[:-1])}:{seconds}'

    if s.endswith('PM'):
        return f'{HOUR_CHANGE[s.split(":")[0]]}:{s.split(":")[1]}:{seconds}'



# if __name__ == '__main__':
def main(s):
    # s = input()

    result = timeConversion(s)

    return result