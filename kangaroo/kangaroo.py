def kangaroo_recursive(x1, v1, x2, v2, jump):
    if jump == 900:
        return 'NO'

    if (x1 + (v1 * jump)) > (x2 + (v2 * jump)):
        return 'NO'

    if (x1 + (v1 * jump)) == (x2 + (v2 * jump)):
        return 'YES'

    return kangaroo_recursive(x1, v1, x2, v2, (jump + 1))

def kangaroo(x1, v1, x2, v2):
    if x2 > x1 and v2 > v1:
        return 'NO'

    return kangaroo_recursive(x1, v1, x2, v2, 1)

# if __name__ == '__main__':
def main(kangaroos_info):
    x1V1X2V2 = kangaroos_info.split()

    x1 = int(x1V1X2V2[0])
    v1 = int(x1V1X2V2[1])
    x2 = int(x1V1X2V2[2])
    v2 = int(x1V1X2V2[3])

    return kangaroo(x1, v1, x2, v2)


"""
Input:
    4523 8092 9419 8076
Output:
    'YES'

0 2 5 3
'NO'

45 7 56 2
'NO'

43 2 70 2
'NO'

4523 8092 9419 8076
'YES'
"""