def utopianTree(n):
    if n == 0:
        return 1

    return utopianTree(n - 1) * 2 if n % 2 != 0 else utopianTree(n - 1) + 1

# if __name__ == '__main__':
def main(periods):
    # t = int(input())

    result = []
    for period in periods:
        # n = int(input())

        result.append(utopianTree(period))

    return result
