def check_scores(position, alice, bob):
    if alice[position] > bob[position]:
            return 'Alice'
    if alice[position] < bob[position]:
            return 'Bob'
    if alice[position] == bob[position]:
            return 'None'

def compareTriplets(a, b):
    alice_score = 0
    bob_score = 0
    for i in range(3):
        assign_to = check_scores(i, a, b)
        if assign_to == 'Alice':
            alice_score += 1
        if assign_to == 'Bob':
            bob_score += 1

    return [alice_score, bob_score]

# if __name__ == '__main__':
def main(a, b):
#     a = list(map(int, input().rstrip().split()))

#     b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    return result