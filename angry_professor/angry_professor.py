def angryProfessor(k, a):
    punctual_students = [student for student in a if student <= 0]

    return 'YES' if len(punctual_students) < k else 'NO'

# if __name__ == '__main__':
def main(k, a):
    # t = int(input())

    # for t_itr in range(t):
    #     nk = input().split()

    #     n = int(nk[0])

    #     k = int(nk[1])

    #     a = list(map(int, input().rstrip().split()))

    result = angryProfessor(k, a)

    return result
