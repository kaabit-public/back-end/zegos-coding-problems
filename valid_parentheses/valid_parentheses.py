"""
    Write a function called valid_parentheses that takes a string of parentheses, and determines if the order of the parentheses is valid.
    The function should return true if the string is valid, and false if it's invalid.
    Examples
    "()"              =>  true
    ")(()))"          =>  false
    "("               =>  false
    "(())((()())())"  =>  true

    Constraints

    0 <= input.length <= 100

    Along with opening (() and closing ()) parenthesis, input may contain any valid ASCII characters. Furthermore, the input string may be empty
    and/or not contain any parentheses at all. Do not treat other forms of brackets as parentheses (e.g. [], {}, <>).
"""

def check_if_is_valid_string(array_of_strings):
    if len(array_of_strings) == 0:
        return True

    if len(array_of_strings) == 1:
        return False

    if array_of_strings[0] == ')' and array_of_strings[-1] == ')':
        return False

    if array_of_strings[0] == '(' and array_of_strings[-1] == '(':
        return False

    if array_of_strings[0] == ')' and array_of_strings[-1] == '(':
        return False

    return True

def remove_continuous_parentheses(array_of_strings):
    position_of_continuous_parentheses = []
    parentheses = []
    for i in range(len(array_of_strings)):
        if i < len(array_of_strings) - 1:
            if array_of_strings[i] == '(' and array_of_strings[i + 1] == ')':
                position_of_continuous_parentheses.append(i)
                position_of_continuous_parentheses.append(i + 1)
                i += 1

    for i in range(len(array_of_strings)):
        if i not in position_of_continuous_parentheses:
            parentheses.append(array_of_strings[i])

    return parentheses

def count_parentheses(array_of_chars):
    count_open_parentheses = 0
    count_close_parentheses = 0

    valid_flag = check_if_is_valid_string(array_of_chars)

    if not valid_flag:
        return False

    for parentheses in array_of_chars:
        if parentheses == '(':
            count_open_parentheses += 1
        else:
            count_close_parentheses += 1

    if count_open_parentheses == count_close_parentheses:
        for i in range(len(array_of_chars)):
            if i < len(array_of_chars) - 2:
                if array_of_chars[i] == ')' and array_of_chars[i + 1] == '(' and array_of_chars[i + 2] != ')':
                    return False
                    break

        return True
    else:
        return False

def valid_parentheses(string):
    if len(string) == 0 or len(string) > 100:
        return True

    valid_array_of_characters = [character for character in string if character in ['(', ')']]

    valid_flag = check_if_is_valid_string(valid_array_of_characters)

    if not valid_flag:
        return False

    new_array_of_chars = remove_continuous_parentheses(valid_array_of_characters)

    parentheses = count_parentheses(new_array_of_chars)

    return parentheses


# print(valid_parentheses("   (")) # False
# print(valid_parentheses(")(()))")) # False
# print(valid_parentheses("(())((()())())")) # False
# print(valid_parentheses("  (")) # False
# print(valid_parentheses(")test")) # False
# print(valid_parentheses("")) # True
# print(valid_parentheses("hi())(")) # False
# print(valid_parentheses("hi)(")) # False
# print(valid_parentheses("hi(hi)()")) # True
# print(valid_parentheses("mbd()lcy)q(gnt(jxz(wu(u)d))hwfvijbh")) # False
# print(valid_parentheses("h(l(n)dq)(j(pt))(czhvp)lkttrs"))