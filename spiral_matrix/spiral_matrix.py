"""
--- Directions
Write a function that accepts an integer N
and returns a NxN spiral matrix.
--- Examples
    matrix(2)
   [[1, 2],
    [4, 3]]
    matrix(3)
   [[1, 2, 3],
    [8, 9, 4],
    [7, 6, 5]]
    matrix(4)
   [[1,  2,  3, 4],
    [12, 13, 14, 5],
    [11, 16, 15, 6],
    [10,  9,  8, 7]]
    matrix(5)
   [[ 1,  2,  3,  4, 5],
    [16, 17, 18, 19, 6],
    [15, 24, 25, 20, 7],
    [14, 23, 22, 21, 8],
    [13, 12, 11, 10, 9]]
"""

def spiral_matrix(n):
    matrix = []
    count = 1

    for i in range(n):
        dummy_data = ' ' * (n - 1)
        matrix.append(dummy_data.split(' '))

    start_row = 0
    start_column = 0
    end_row = n - 1
    end_column = n - 1

    while start_column <= end_column and start_row <= end_row:
        for i in range(start_column, end_column + 1):
            matrix[start_row][i] = count
            count += 1

        start_row += 1

        for i in range(start_row, end_row + 1):
            matrix[i][end_row] = count
            count += 1

        end_column -= 1

        for i in range(end_column, start_column - 1, -1):
            matrix[end_row][i] = count
            count += 1

        end_row -= 1

        for i in range(end_row, start_row - 1, -1):
            matrix[i][start_column] = count
            count += 1

        start_column += 1

    return matrix

def main(n):

    return spiral_matrix(n)
