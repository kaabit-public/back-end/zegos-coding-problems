def beautifulDays(i, j, k):
    beautiful_day = []
    for i in range(i, j + 1):
        if (i - int(str(i)[::-1])) % k == 0:
            beautiful_day.append(i)

    return len(beautiful_day)

# if __name__ == '__main__':
def main(i, j, k):
    # ijk = input().split()
    # i = int(ijk[0])
    # j = int(ijk[1])
    # k = int(ijk[2])

    return beautifulDays(i, j, k)