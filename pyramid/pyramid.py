# --- Directions
# Write a function that accepts a positive number N.
# The function should console log a pyramid shape
# with N levels using the # character.  Make sure the
# pyramid has spaces on both the left *and* right hand sides
# --- Examples
#   pyramid(1)
#       '#'
#   pyramid(2)
#       ' # '
#       '###'
#   pyramid(3)
#       '  #  '
#       ' ### '
#       '#####'

def pyramid(n):
    pattern = []
    for i in range(n):
        result = '#' * (n + (n - 1))
        result = result.replace('#', ' ', (n - (i + 1)))
        result = result[::-1]
        result = result.replace('#', ' ', (n - (i + 1)))
        pattern.append(result)

    return pattern

# if __name__ == '__main__':
def main(n):
    # n = int(input('Ingrese un número: '))

    return pyramid(n)