def get_mins(arr):
    values = arr.copy()
    minimum_values = []
    for i in range(4):
        minimum_values.append(min(values))
        values.remove(min(values))
    
    return minimum_values

def get_maxs(arr):
    values = arr.copy()
    maximum_values = []
    for i in range(4):
        maximum_values.append(max(values))
        values.remove(max(values))
    
    return maximum_values

def miniMaxSum(arr):
    minimums = get_mins(arr)
    maximums = get_maxs(arr)

    max_sum = sum(maximums)
    min_sum = sum(minimums)

    return[min_sum, max_sum]

# if __name__ == '__main__':
def main(arr):
    # arr = list(map(int, input().rstrip().split()))

    return miniMaxSum(arr)
