"""
    I forgot to copy the problem description.
"""
def get_occurrency(list_of_dicts):
    counter = 0
    for dictionary in list_of_dicts:
            for key, value in dictionary.items():
                if key == 'occurrency':
                    if dictionary[key] > 1:
                        counter += 1
    
    return counter

def count_occurrency_of_chars(text_to_array, set_of_chars, list_of_dicts):
    counter = 0
    for char in set_of_chars:
        for i in range(len(text_to_array)):
            if char == text_to_array[i]:
                counter += 1
        for dictionary in list_of_dicts:
            for key, value in dictionary.items():
                if key == 'value':
                    if dictionary[key] == char:
                        dictionary['occurrency'] = counter
        counter = 0

    return list_of_dicts

def create_list_of_dicts(set_of_chars):
    return [{'value': char, 'occurrency': 0} for char in set_of_chars]

def duplicate_count(text):
    text_to_array = [str(char.lower()) for char in text]
    set_of_chars = set(text_to_array)
    list_of_dicts = create_list_of_dicts(set_of_chars)
    list_of_dicts = count_occurrency_of_chars(text_to_array, set_of_chars, list_of_dicts)
    count_occurrency = get_occurrency(list_of_dicts)
    return count_occurrency


# def duplicate_count(s):
#     return len([c for c in set(s.lower()) if s.lower().count(c)>1])


# print(duplicate_count("abcde"))
# print(duplicate_count("abcdea"))
# print(duplicate_count("indivisibility"))
# print(duplicate_count("aA11"))
# print(duplicate_count("ABBA"))