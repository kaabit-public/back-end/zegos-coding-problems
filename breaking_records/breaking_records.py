def get_highest_scores(scores):
    first_score = scores[0]
    current_highest_score = 0

    best_scores = []
    for index in range(1, len(scores)):
        if scores[index] > current_highest_score and scores[index] > first_score:
            current_highest_score = scores[index]
            best_scores.append(scores[index])

    return list(set(best_scores))

def get_lowest_scores(scores):
    first_score = scores[0]
    current_lowest_score = max(scores)

    lowest_scores = []
    for index in range(1, len(scores)):
        if scores[index] < first_score and scores[index] < current_lowest_score:
            current_lowest_score = scores[index]
            lowest_scores.append(scores[index])

    return list(set(lowest_scores))

def breakingRecords(scores):
    highest_scores = get_highest_scores(scores)
    lowest_scores = get_lowest_scores(scores)

    return [len(highest_scores), len(lowest_scores)]

    # return list(set([score for score in scores if score > first_score]))

# if __name__ == '__main__':
def main(scores_input):
    scores = list(map(int, scores_input.split()))

    return breakingRecords(scores)
