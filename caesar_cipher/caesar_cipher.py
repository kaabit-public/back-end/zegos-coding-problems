def generate_alphabet(k, alphabet) -> list:
    new_alphabet = alphabet.copy()
    for i in range(k):
        first_element = new_alphabet.pop(0)
        new_alphabet.insert(len(alphabet), first_element)

    return new_alphabet

def generate_dict(ordered_alphabet, alphabet):
    cipher_dict = {}

    for idx, character in enumerate(ordered_alphabet):
        cipher_dict[character] = alphabet[idx]

    return cipher_dict

def generate_cipher_string(string, dictionary):
    cipher_string = ''

    for character in string:
        if (character.lower() in dictionary.keys()):
            cipher_string += dictionary[character.lower()].upper() if character.isupper() else dictionary[character.lower()]
        else:
            cipher_string += character

    return cipher_string

def caesarCipher(s, k) -> str:
    ordered_alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    new_alphabet = generate_alphabet(k, ordered_alphabet)

    cipher_dict = generate_dict(ordered_alphabet, new_alphabet)

    cipher_string = generate_cipher_string(s, cipher_dict)

    return cipher_string

# if __name__ == '__main__':
def main(s, k):
    # n = int(input())

    # s = input()

    # k = int(input())

    return caesarCipher(s, k)