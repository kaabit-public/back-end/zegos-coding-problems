def divisibleSumPairs(n, k, ar):
    count = 0
    for i in range(len(ar)):
        for j in range(i + 1, len(ar)):
            if (ar[i] + ar[j]) % k == 0:
                count += 1

    return count

# if __name__ == '__main__':
def main(nk_input, ar):
    nk = nk_input.split()
    n = int(nk[0])
    k = int(nk[1])

    # ar = list(map(int, input().rstrip().split()))

    return divisibleSumPairs(n, k, ar)
