def factorial(n):
    if n == 1:
        return 1

    return factorial(n - 1) * n

def extraLongFactorials(n):
    return factorial(n)

# if __name__ == '__main__':
def main(n):
    # n = int(input())

    return extraLongFactorials(n)
