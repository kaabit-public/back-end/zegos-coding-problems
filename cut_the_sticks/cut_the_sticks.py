def cutTheSticks(arr):
    sticks_cutted = []
    while len(arr) > 0:
        minimun = min(arr)
        sticks_cutted.append(len(arr))

        arr = [element - minimun for element in arr if element - minimun > 0]

    return sticks_cutted


# if __name__ == '__main__':
def main(arr):
    # n = int(input())

    # arr = list(map(int, input().rstrip().split()))

    return cutTheSticks(arr)