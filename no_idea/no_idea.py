"""

    There is an array of 'n' integers. There are also 2 disjoint sets, A and B, each containing 'm' integers. 
    You like all the integers in set A and dislike all the integers in set B. Your initial happiness is 0. 
    For each 'i' integer in the array, if 'i' belongs A, you add 1 to your happiness. 
    If 'i' belongs B, you add -1 to your happiness. 
    Otherwise, your happiness does not change. Output your final happiness at the end.

    Note: Since A and B are sets, they have no repeated elements. However, the array might contain duplicate elements.

    Constraints
    1 <= n <= 10^5
    1 <= m <= 10^5
    1 <= Any integer in the input <= 10^9

    Input Format:
    The first line contains integers 'n' and 'm' separated by a space.
    The second line contains 'n' integers, the elements of the array.
    The third and fourth lines contain 'm' integers, A and B, respectively.

    Output Format:
    Output a single integer, your total happiness.

    Sample Input
    3 2
    1 5 3
    3 1
    5 7

    Sample Output:
    1

    Explanation:
    You gain 1 unit of happiness for elements 3 and 1 in set A. You lose 1 unit for 5 in set B. 
    The element 7 in set B does not exist in the array so it is not included in the calculation.
    Hence, the total happiness is 2 - 1 = 1.

"""
# n, m = input().split()

# sc_ar = input().split()

# A = set(input().split())
# B = set(input().split())
# print(sum([(i in A) - (i in B) for i in sc_ar]))

def add_happy_points(list_numbers, happy_set):
    happy_points = 0
    for i in range(len(list_numbers)):
        if list_numbers[i] in happy_set:
            happy_points += 1

    return happy_points

def add_sad_points(list_numbers, sad_set):
    sad_points = 0
    for i in range(len(list_numbers)):
        if list_numbers[i] in sad_set:
            sad_points += 1

    return sad_points

def main(list_numbers, set_a, set_b):
    happy_points = add_happy_points(list_numbers, set_a)
    sad_points = add_sad_points(list_numbers, set_b)

    # if sad_points > happy_points:
    #     return 0
    # else:
    return happy_points - sad_points

# if __name__ == '__main__':
def run(n_and_m, n, a, b):
    # n_and_m = input()
    splitted_input = n_and_m.split()

    elements_array = [None] * int(splitted_input[0])
    list1 = [None] * int(splitted_input[1])
    list2 = [None] * int(splitted_input[1])
    
    # n = input()
    splitted_n = n.split()
    for _ in range(int(splitted_input[0])):
        elements_array[_] = int(splitted_n[_])

    # a = input()
    splitted_a = a.split()
    for _ in range(int(splitted_input[1])):
        list1[_] = int(splitted_a[_])

    # b = input()
    splitted_b = b.split()
    for _ in range(int(splitted_input[1])):
        list2[_] = int(splitted_b[_])

    list1_unique_elements = set(list1)
    list2_unique_elements = set(list2)

    points = main(elements_array, list1_unique_elements, list2_unique_elements)
    return points
    # print(points)
