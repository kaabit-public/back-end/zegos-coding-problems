def repeatedString(s, n):
    full_string = s.count('a') * (n // len(s))

    partial_string = 0
    if n % len(s) != 0:
        partial_string = s[:n % len(s)].count('a')

    return full_string + partial_string

# if __name__ == '__main__':
def main(s, n):
    # s = input()

    # n = int(input())

    return repeatedString(s, n)
