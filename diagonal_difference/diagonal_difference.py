def primary_diagonal(arr):
    row_length = len(arr[0])
    total = 0

    for i in range(row_length):
        total += arr[i][i]

    return total

def secondary_diagonal(arr):
    row_length = len(arr[0]) - 1
    total = 0
    aux = 0

    for i in range(row_length, -1, -1):
        total += arr[aux][i]
        aux += 1

    return total

def diagonalDifference(arr):
    sum_primary_diagonal = primary_diagonal(arr)
    sum_secondary_diagonal = secondary_diagonal(arr)

    return abs(sum_primary_diagonal - sum_secondary_diagonal)

# if __name__ == '__main__':
def main(arr):
    # n = int(input().strip())

    # arr = []

    # for _ in range(n):
        # arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)
    # print(result)
    return result
