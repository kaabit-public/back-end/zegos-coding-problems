"""

    You are given an array (which will have a length of at least 3, but could be very large) containing integers. 
    The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. 
    Write a method that takes the array as an argument and returns this "outlier" N.

    print(find_outlier([2, 4, 6, 8, 10, 3])) # 3
    print(find_outlier([2, 4, 0, 100, 4, 11, 2602, 36])) #11
    print(find_outlier([160, 3, 1719, 19, 11, 13, -21])) #160

"""

def check_if_last_position_is_outlier(integers, is_even):
    if integers[-1] % 2 == 0  and is_even:
        return False
    elif integers[-1] % 2 != 0 and not is_even:
        return False
    elif integers[-1] % 2 != 0  and is_even:
        return True
    elif integers[-1] % 2 == 0 and not is_even:
        return True

def check_if_first_position_is_outlier(integers, is_even):
    if integers[0] % 2 == 0  and is_even:
        return False
    elif integers[0] % 2 != 0 and not is_even:
        return False
    elif integers[0] % 2 != 0  and is_even:
        return True
    elif integers[0] % 2 == 0 and not is_even:
        return True

def check_if_is_even(integers):
    count_even, count_odd = 0, 0
    for i in range(0, len(integers)):
        if integers[i] % 2 == 0:
            count_even += 1
        else:
            count_odd += 1
    
    return True if count_even > count_odd else False

def find_outlier(integers):
    is_even = check_if_is_even(integers)
    is_the_first_outlier = check_if_first_position_is_outlier(integers, is_even)
    is_the_last_outlier = check_if_last_position_is_outlier(integers, is_even)
    if is_the_first_outlier:
        return integers[0]
    elif is_the_last_outlier:
        return integers[-1]
    else:
        if is_even:
            return [element for element in integers if element % 2 != 0][0]
        else:
            return [element for element in integers if element % 2 == 0][0]



# print(find_outlier([2, 4, 6, 8, 10, 3])) # 3
# print(find_outlier([2, 4, 0, 100, 4, 11, 2602, 36])) #11
# print(find_outlier([160, 3, 1719, 19, 11, 13, -21])) #160