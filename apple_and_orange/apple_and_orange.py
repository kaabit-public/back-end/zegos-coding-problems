def countApplesAndOranges(s, t, a, b, apples, oranges):
    apples_thrown = [apple + a for apple in apples]
    oranges_thrown = [orange + b for orange in oranges]

    apples_in_area = [apple for apple in apples_thrown if apple >= s and apple <= t]
    oranges_in_area = [orange for orange in oranges_thrown if orange >= s and orange <= t]

    return [len(apples_in_area), len(oranges_in_area)]

# if __name__ == '__main__':
def main(st_input, ab_input, mn_input, apples_input, oranges_input):
    st = st_input.split()
    s = int(st[0])
    t = int(st[1])

    ab = ab_input.split()
    a = int(ab[0])
    b = int(ab[1])

    mn = mn_input.split()
    m = int(mn[0])
    n = int(mn[1])

    apples = list(map(int, apples_input.split()))
    oranges = list(map(int, oranges_input.split()))

    return countApplesAndOranges(s, t, a, b, apples, oranges)
